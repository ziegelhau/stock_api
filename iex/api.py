import logging
import os
from datetime import datetime as dt2
from urllib.error import HTTPError

import pandas as pd
import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)


# quote,news,chart
# chart is historic data
# types = 'quote,chart'
# url = base_url
# def get_historical_prices_for_past_month(base_url, symbols, token_str):
# GET /stock/{symbol}/chart/{range}/{date}
class API:
    def __init__(self, use_real_paid_for_data):
        if use_real_paid_for_data:
            self.base_url = 'https://cloud.iexapis.com/stable'
            self.token_str = f'token={os.environ.get("IEX REAL TOKEN")}'
        else:
            self.base_url = 'https://sandbox.iexapis.com/stable'
            self.token_str = f'token={os.environ.get("IEX TEST TOKEN")}'

    def get_historical_prices(self, symbols: list, range, types='chart'):
        # GET /stock/{symbol}/chart/{range}/{date}
        if type(symbols) != list:
            symbols = [symbols]
        if len(symbols) == 1:
            raise ValueError('need to do multiple right now')  # symbols_str = symbols[0]
        else:
            symbols_str = f'batch?symbols={",".join(symbols)}'
        url = f'{self.base_url}/stock/market/{symbols_str}&types=chart&chartCloseOnly=true&range={range}' \
              f'&{self.token_str}&format=csv'
        print(url)
        # url = f'{self.base_url}/stock/market/{symbols_str}&types=recommendation-trends&{self.token_str}'  # &format=csv'
        logger.debug(url)
        # r = requests.get(url)
        return pd.read_csv(url)

    # GET /stock/{symbol}/recommendation-trends
    # https://iexcloud.io/docs/api/#analyst-recommendations
    def get_analyst_recommendations(self, symbols: list):
        # /stock/aapl/batch?types=quote,news,chart&range=1m&last=10
        # /stock/market/batch?symbols=aapl,fb,tsla&types=quote,news,chart&range=1m&last=5
        # https://sandbox.iexapis.com/stable/stock/market/batch?symbols=aapl,fb&types=recommendation-trends&token=mytoken
        # https://iexcloud.io/docs/api/#analyst-recommendations
        # /stock/{symbol}/recommendation-trends
        # https://sandbox.iexapis.com/stable/stock/market/batch?symbols=aapl,fb&types=recommendation-trends&token=mytoken
        # if type(symbols) != list:
        #     symbols = [symbols]
        #     # https://sandbox.iexapis.com/stable/stock/twtr/price-target/?token=mytoken&format=csv
        #     # https://sandbox.iexapis.com/stable/stock/twtr/recommendation-trends/?token=mytoken
        #     url = f'{self.base_url}/stock/{symbols}/recommendation-trends/?{self.token_str}'  # &format=csv'
        #     logger.info(url)
        #     # return pd.read_csv(url)
        #     return requests.get(url)
        symbols_str = f'batch?symbols={",".join(symbols)}'
        url = f'{self.base_url}/stock/market/{symbols_str}&types=recommendation-trends&{self.token_str}'  # &format=csv'
        logger.debug(url)
        r = requests.get(url)
        logger.debug(r.status_code)
        if r.status_code != 200:
            raise ValueError(f'{r.status_code}: {r.reason}')
        # return pd.read_csv(url)
        return r

    def get_analyst_recommendations_and_return_df(self, symbol: str):
        """
        Just call get_analyst_recommendations and then smooth out data to something human readable
        Convert unix timestamps to date times
        Add column totalRatings

        Columns in response 'ratingBuy', 'ratingOverweight', 'ratingHold', 'ratingUnderweight',
       'ratingSell', 'ratingNone', 'ratingScaleMark', 'consensusStartDate',
       'consensusEndDate', 'corporateActionsAppliedDate',
        """
        r = self.get_analyst_recommendations(symbol)
        r_json = r.json()
        df = pd.DataFrame()
        # df = pd.DataFrame(
        #     columns=['ratingBuy', 'ratingOverweight', 'ratingHold', 'ratingUnderweight', 'ratingSell', 'ratingNone',
        #              'ratingScaleMark', 'consensusStartDate', 'consensusEndDate', 'corporateActionsAppliedDate'])
        for k, v in r_json.items():
            df_temp = pd.DataFrame(v['recommendation-trends'])
            df_temp['symbol'] = k
            df = df.append(df_temp)
        if df.empty:
            return df
        date_cols = ['consensusStartDate', 'consensusEndDate', 'corporateActionsAppliedDate']
        # Is in milliseconds so divide by 1000 to get it in minutes
        # This cn be cleaned up a bit
        df[date_cols] = df[date_cols] / 1000
        year_2050 = 2524608000  # NaN value means value is still applicable, so just use that
        df[date_cols] = df[date_cols].fillna(year_2050)
        df[date_cols] = df[date_cols].applymap(lambda x: dt2.fromtimestamp(x))
        df['total_ratings'] = df[
            ['ratingBuy', 'ratingOverweight', 'ratingHold', 'ratingUnderweight', 'ratingSell', 'ratingNone']].sum(
            axis=1)
        logger.debug(df.to_string())
        return df

    def get_latest_analyst_recommendations_as_series(self, symbols):
        multi_symbols = self.create_batch_calls(symbols)
        df = pd.DataFrame()
        # df = pd.DataFrame(
        #     columns=['symbol', 'ratingBuy', 'ratingOverweight', 'ratingHold', 'ratingUnderweight', 'ratingSell',
        #              'ratingNone',
        #              'ratingScaleMark', 'consensusStartDate', 'consensusEndDate', 'corporateActionsAppliedDate',
        #              'total_ratings', 'valid since'])
        for symbols in multi_symbols:
            # TODO: need to add a check that the consensusEndDate is within the correct range
            df_t = self.get_analyst_recommendations_and_return_df(symbols)
            if df_t.empty:
                logger.info(f'{symbols}: no analyst data')
                continue
            df_t['valid since'] = (dt2.now() - df_t['consensusStartDate']).dt.days
            df = df.append(df_t.sort_values('consensusStartDate').groupby('symbol').tail(1))
        return df

    # GET /stock/{symbol}/options/{expiration}/{optionSide?}
    # https://iexcloud.io/docs/api/#options
    # def get_options(symbol, expiry_date):
    #
    #     # Do not have intraday data for past couple days
    #     # https://iexcloud.io/docs/api/#intraday-prices
    #     # Get intraday prices
    #     # YYYYMMDD
    #     # https://sandbox.iexapis.com/stable/stock/aapl/options?token=mytoken
    #     # https://sandbox.iexapis.com/stable/stock/aapl/options/201906?token=mytoken
    #     url = f'{base_url}/stock/{symbol}/options/{expiry_date}?&{token_str}'
    #     logger.info(url)
    #     r = requests.get(url=url)
    #     logger.info(r.status_code)
    #     return r

    # GET /stock/{symbol}/price-target
    # https://iexcloud.io/docs/api/#price-target
    def get_price_target(self, symbols):
        # # https://sandbox.iexapis.com/stable/stock/market/batch?symbols=aapl,fb&types=recommendation-trends&token=mytoken
        # if type(symbols) != list:
        #     symbols = [symbols]
        #     # https://sandbox.iexapis.com/stable/stock/twtr/price-target/?token=mytoken&format=csv
        #     url = f'{self.base_url}/stock/{symbols}/price-target/?{self.token_str}&format=csv'
        #     logger.info(url)
        #     return pd.read_csv(url)

        multi_symbols = self.create_batch_calls(symbols)
        df = pd.DataFrame()
        for symbols in multi_symbols:
            symbols_str = f'batch?symbols={",".join(symbols)}'
            url = f'{self.base_url}/stock/market/{symbols_str}&types=price-target&{self.token_str}&format=csv'
            logger.debug(url)
            try:
                df_t = pd.read_csv(url)
            except HTTPError:
                logger.info(f'{symbols}: no target price data')
                return df
            # import time
            # time.sleep(12)
            df = df.append(df_t)  # df_t = api.get_historical_prices(symbols, range)  # df = df.append(df_t)
        # r = requests.get(url)
        df['time created'] = dt2.today()
        return df

    def create_batch_calls(self, all_symbols: list):
        i = 0
        num_symbols = len(all_symbols)
        symbols = []
        while i < num_symbols:
            if i + 40 > num_symbols:
                end = num_symbols
            else:
                end = i + 40
            # symbols = all_symbols[i:end]
            symbols.append(all_symbols[i:end])
            # df_t = api.get_historical_prices(symbols, range)
            # df = df.append(df_t)
            # logger.info(f'finished batch {end}')
            i += 40
        return symbols
