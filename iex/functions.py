from pandas_datareader.iex.ref import SymbolsReader


def validate_symbols(symbols, no_data_syms):
    # Check that symbols are in IEX
    # Discard symbols if the are an int
    symbols = [x for x in symbols if not isinstance(x, int)]
    iex_symbols = SymbolsReader().read()['symbol'].to_list()
    valid_symbols = []
    for symbol in symbols:
        if (symbol in iex_symbols) and (symbol not in no_data_syms):
            valid_symbols.append(symbol)
    return valid_symbols
