# df = get_arnie_criteria(df)
def get_arnie_criteria(df, criteria=ARNIE_CRITERIA):
    """
    Arnie's strategy
    Buy criteria:
        Buy 1% prior to pivot
        Buy stocks within the 1,2, or 3rd stage
        composite rating over 90
        rs rating over 85
        No.Fund has to be going up (number of mutual funds owning this)
            every friday it is updated so good thing to check on saturday
        U/D Vol Ratio: 1.1 or greater
        Volume is greater than 20% above normally traded volume
        Industry Group
            Conflicting information here
            Has to be in the industry groups top 20
            has to be within the top 2 or 3 of it's industry and sector
        William J O'Neil 7/9 Checklist
    Sell criteria:
        20-25% from pivot
    Stop Loss criteria:
        5% below pivot
    """
    df = get_checklist_score(df, 'arnie', ARNIE_COLS)
    df['arnie criteria'] = df['arnie score'] >= criteria
    return df


def get_checklist_score(df, name, cols):
    df[f'{name} pass'] = df[cols].sum(axis=1)
    df[f'{name} fail'] = len(cols) - df[f'{name} pass'] - df[cols].isna().sum(axis=1)
    df[f'{name} score'] = df[f'{name} pass'] / (df[f'{name} pass'] + df[f'{name} fail'])
    return df


def get_oneil_criteria(df, criteria=ONEIL_CRITERIA):
    df = get_checklist_score(df, 'oneil', ONEIL_COLS)
    df['oneil criteria'] = df['oneil score'] >= criteria
    return df


def add_criteria_columns(df):
    # TODO: is just for Arnie and Oneill Criteria
    #  Should just put these directly in their scores
    Criteria('current price ≥ 10', M.cur_price, 9.9, operator.gt)
    Criteria('vol % chg vs 50-day ≥ 20', M.vol_per_chg_50_day, 19, operator.gt)
    Criteria('eps rating ≥ 80', M.eps_rating, 79, operator.gt)
    Criteria('rs rating ≥ 80', M.rs_rating, 79, operator.gt)
    Criteria('rs rating ≥ 85', M.rs_rating, 84, operator.gt)
    Criteria('a/d rating a,b,c', M.ad_rating_int, 1, operator.gt)
    Criteria('eps % chg last qtr (-/+) ≥ 20', M.eps_per_chg_lst_qtr, 19.9, operator.gt)
    Criteria('sales % chg lst qtr ≥ 20', M.sales_per_chg_lst_qtr, 19.9, operator.gt)
    Criteria('current price ≥ 10', M.cur_price, 9.9, operator.gt)
    Criteria('% off high ≥ -15', '% off high', -16, operator.gt)
    Criteria('50-day avg vol (1000s) ≥ 100', M.vol_50_day_avg, 99, operator.gt)
    Criteria('number of funds ≥ 5', M.num_funds, 4, operator.gt)
    Criteria('comp rating ≥ 90', M.comp_rating, 89, operator.gt)
    Criteria('ind group rank ≤ 20', M.ind_group_rank, 21, operator.lt)
    Criteria('up/down vol ≥ 1.1', M.up_down_vol, 1, operator.gt)
    Criteria('vol % chg vs 50-day ≥ 20', M.vol_per_chg_50_day, 19, operator.gt)
    scaling = 'step'
    for criteria in Criteria.s:
        df[f'{criteria.name}'] = criteria.apply(df[criteria.col], scaling)
    return df


# def remove_symbols_from_df(df, symbols_list):
#     for symbol in symbols_list:
#         df = df.drop(df[df['symbol'] == symbol].index)
#     return df

def create_td_file_name_given_account(td_account=Fname.td_in):
    """
    TD Account file ex: 363BY6F-holdings-04-Jun-2020.csv
    To get  the latest file need to find the latest one by date
    Lets just assume for now that we have it
    """
    fname = f'{td_account}-holdings-{dt2.now().strftime("%d-%b-%Y")}'
    files = os.listdir(Folder.downloads.value)
    csv_file = find_latest_files_in_folder(files, fname)[0]
    logger.debug(f'TD file={csv_file}')
    return csv_file


def move_item_to_start_of_list(in_cols, col):
    # Not currently used
    cols = in_cols[:]
    cols.remove(col)
    cols.insert(0, col)
    return cols


def add_scaled_columns(df):
    gt_80 = MultiStep([Map(60, 80, 0.4, 0.8), Map(80, 100, 0.8, 1)], min=60, limits_first=True)
    df = gt_80.apply(df, M.eps_rating)
    df = MultiStep([Map(-1, 1, 0.4, 0.8), Map(2, 10, 0.8, 1)], min=-1).apply(df, M.ad_rating_int)
    gt_20 = MultiStep([Map(0, 20, 0.4, 0.8), Map(20, 40, 0.8, 1)], min=0, max=40)
    df = gt_20.apply(df, M.eps_per_chg_lst_qtr)
    df = gt_20.apply(df, M.sales_per_chg_lst_qtr)
    # TODO: need to not overwrite the original columns
    col_vals = {M.cur_price: 10, '% off high': -15, M.vol_50_day_avg: 100, M.num_funds: 5}
    for col, val in col_vals.items():
        df.loc[col + ' criteria'] = 0
        mask = df[col] < val
        df.loc[~mask, col + ' criteria'] = 1
    gt_90 = MultiStep([Map(80, 90, 0.7, 0.9), Map(90, 100, 0.9, 1)], min=80, limits_first=True)
    df = gt_90.apply(df, M.comp_rating)
    df[M.comp_rating] = df[M.comp_rating].astype(float)
    df[M.ind_group_rank] = 101 - df[M.ind_group_rank]
    df = MultiStep([Map(50, 80, 0.2, 0.8), Map(80, 100, 0.8, 1)], min=50, limits_first=True).apply(df, M.ind_group_rank)
    df = MultiStep([Map(1.1, 3, 0, 1)], min=1.1, new_min=0, max=3, limits_first=True).apply(df, M.up_down_vol)
    df = MultiStep([Map(0.01, 20, 0.4, 0.8), Map(20, 100, 0.8, 1)], min=0, new_min=0, max=100, new_max=1).apply(df,
                                                                                                                M.vol_per_chg_50_day)
    return df


def create_safe_sector_column(df, good_sectors=GOOD_SECTORS, good_industries=GOOD_INDUSTRIES):
    df['safe business'] = df['sector'].isin(good_sectors) | df[M.ind_name].str.contains('|'.join(good_industries),
                                                                                        na=False)
    df['safe business'] = df['safe business'].replace(False, '')
    return df
