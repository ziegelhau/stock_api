import logging
import time

from selenium.common.exceptions import NoSuchWindowException
from selenium.webdriver.support.wait import WebDriverWait

from misc.settings import DEFAULT_LOAD_TIME
from scraper.settings import CHROMEDRIVER

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

from selenium import webdriver

options = webdriver.ChromeOptions()
chrome_profile = 'C:/Users/johnm/AppData/Local/Google/Chrome/User Data/profile1'
# edge_profile = "C:/Users/johnm/AppData/Local/Microsoft/Edge/User Data/"
options.add_argument('user-data-dir=' + chrome_profile)  # Path to your chrome


# Getting redirected to the login page when I use headless for some reason
# options.headless = True
# # options.add_argument('--disable-gpu')
# options.add_argument('--no-sandbox')


class Driver(webdriver.Edge):
    def __init__(self, driver=None):
        # TODO: make driverFactory
        if driver is None:
            self.driver = webdriver.Chrome(executable_path=CHROMEDRIVER, chrome_options=options)
        else:
            self.driver = driver

        self.wait = WebDriverWait(self.driver, DEFAULT_LOAD_TIME)

    def open_edge_first_url(self, driver, url):
        # Edge can open the window too fast and crash itself, if it raises an error, try again in half a second
        try:
            self.driver.get(url)
        except NoSuchWindowException:
            time.sleep(0.5)
            self.driver.get(url)

    def xpath(self, element, prop, name):
        """
        Create xpath string given the element, element type/property, and the name of that property
        """
        return f'//{element}[@{prop}="{name}"]'

    def get_el_by_xpath(self, element, prop, name):
        return self.wait.until(lambda driver: driver.find_element_by_xpath(self.xpath(element, prop, name)))
