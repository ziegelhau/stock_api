import logging
import time
from datetime import datetime as dt2
from timeit import default_timer as timer

import pandas as pd
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

from misc.settings import Fname
from misc.utils import create_latest_csv
from scraper.driver import Driver
from scraper.settings import URL_BASE

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

# ----------INPUT VARIABLES---------- #
all_filename = 'breaking_out_today.csv'
recent_filename = 'all.csv'


# ----------INPUT VARIABLES---------- #

def scraper(webdriver=None, symbols_list=None, time_stock_on_screen=0.7):
    logger.debug('running webscraper')
    start = timer()
    # TODO: make get_or_create_driver()
    if webdriver is None:
        webdriver = Driver()
    wait = webdriver.wait
    webdriver.open_edge_first_url(webdriver, URL_BASE)
    df = pd.DataFrame()
    list_len = len(symbols_list)
    c = 0
    # Go to Nasdaq first because we won't be analyzing that ever
    # Nasdaq has a blank company description
    old_company_name = WebDriverWait(webdriver.driver, 10).until(
        lambda d: d.find_element_by_xpath('//span[@class="companyInfCoName"]')).text
    if old_company_name != '':
        symbols_input_box = wait.until(lambda d: d.find_element_by_class_name('symbolsInput'))
        symbols_input_box.clear()
        symbols_input_box.send_keys('0NDQC')
        symbols_input_box.send_keys(Keys.ENTER)
        old_company_name = WebDriverWait(webdriver.driver, 10).until(
            lambda d: d.find_element_by_xpath('//span[@class="companyInfCoName"]')).text
    # logger.info(f'old company name={old_company_name}')
    time.sleep(0.5)
    for symbol in symbols_list:
        time.sleep(time_stock_on_screen)
        c += 1
        logger.info(f'{c} of {list_len}: {symbol}')
        in_dict = {'timestamp': dt2.now(), 'symbol': symbol}
        symbols_input_box = wait.until(lambda driver: driver.find_element_by_class_name('symbolsInput'))
        symbols_input_box.clear()
        symbols_input_box.send_keys(symbol)
        symbols_input_box.send_keys(Keys.ENTER)
        # Check that it actually changed the page
        company_name = wait.until(lambda d: d.find_element_by_xpath('//span[@class="companyInfCoName"]')).text
        while old_company_name == company_name:
            logger.debug(old_company_name)
            try:
                company_name = wait.until(lambda d: d.find_element_by_xpath('//span[@class="companyInfCoName"]')).text
            except StaleElementReferenceException:
                pass
            logger.debug(company_name)
            time.sleep(0.5)
        old_company_name = company_name
        time.sleep(1)
        # Cases
        # +12% from Pivot in 6 days
        # -2% from Pivot in 5 days
        # 9% to Pivot
        try:
            pivot_and_days = wait.until(lambda d: d.find_element_by_xpath('//div[contains(text(), "Pivot")]'))
        except TimeoutException:
            symbols_input_box.send_keys(Keys.ENTER)
            time.sleep(1)
            try:
                pivot_and_days = wait.until(lambda d: d.find_element_by_xpath('//div[contains(text(), "Pivot")]'))
            except TimeoutException:
                continue
        while pivot_and_days.text == '':
            pivot_and_days = wait.until(lambda d: d.find_element_by_xpath('//div[contains(text(), "Pivot")]'))
            time.sleep(0.5)
        try:
            percent_with_symbol, x_days = pivot_and_days.text.split(" from Pivot in ")
            # Remove percent symbol
            percent = float(percent_with_symbol[:-1]) * 0.01
            days, _ = x_days.split(" day")
        except ValueError:
            percent = int(pivot_and_days.text.split(" to Pivot")[0][:-1]) * -0.01
            days = None
        logger.debug(f'percent: {percent}')
        in_dict['pivot percent away'] = percent
        in_dict['days since pivot'] = days
        df_single = pd.DataFrame([in_dict])
        df_single.to_csv(Fname.marketsmith_all, mode='a', index=False, header=False)
        df = df.append(df_single, ignore_index=True)
        create_latest_csv(Fname.marketsmith_all, Fname.marketsmith_recent)

    end = timer()
    elapsed = str(end - start)
    logger.info(f'Run time in seconds: {elapsed}')
    return webdriver.driver


def download_latest_marketsmith_list(driver=None):
    """
    Open analysis, click, tooltip dropdown
    Hover over export in
    Export as CSV
    """
    if driver is None:
        webdriver = Driver()
    wait = webdriver.wait  # create specific instance of webdriverwait with shorter timeout
    webdriver.open_edge_first_url(webdriver.driver, URL_BASE)
    # TODO: wait until element is interactable
    time.sleep(5)
    wait.until(lambda d: d.find_element_by_xpath(webdriver.xpath('div', 'class', "LMBtnTools "
                                                                                 "toolTip-help"))).click()
    export_el = wait.until(lambda d: d.find_element_by_xpath(webdriver.xpath('li', 'class', "exportIn")))
    time.sleep(3)
    hover = ActionChains(webdriver.driver).move_to_element(export_el)
    hover.perform()
    time.sleep(3)
    wait.until(lambda d: d.find_element_by_xpath(webdriver.xpath('li', 'class', "exportCSV"))).click()
    # TODO: wait until it says completed download
    time.sleep(2)
