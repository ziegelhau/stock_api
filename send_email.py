from datetime import datetime as dt2

from main import main
from misc import settings

# class Settings:
#     RUN_WEBSCRAPER = False
#     EMAIL_JAMES = True
#     GET_LATEST_MARKETSMITH_FILE = False


settings.RUN_WEBSCRAPER = False
settings.EMAIL_JAMES = True
settings.GET_LATEST_MARKETSMITH_FILE = False


def run_continuously():
    # while True:
    main(settings)
    print(f'ran at {dt2.now()}')  # time.sleep(3600)  # run every hour


run_continuously()
