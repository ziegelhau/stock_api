import pandas as pd
from dateutil.utils import today

from analysis.utils import get_iex_df
from iex.functions import validate_symbols
from misc.settings import Fname


def get_analyst_data(no_data_syms):
    in_df = pd.read_csv(Fname.buy)
    # Don't think I need to merge with iex_df, can probably just use pre buy
    # in_df = in_df.drop('time created', axis=1)
    iex_df = pd.read_csv('tables/iex analysts.csv')
    # Find when analysts were last updated
    df = in_df.merge(iex_df, on='symbol', how='left')
    # Only updated if analysis is older than 5 days
    df.loc[df['time created'].isna(), 'time created'] = '1970-01-01'
    symbols = df.loc[(today() - pd.to_datetime(df['time created'])).dt.days >= 5]['symbol'].to_list()
    valid_symbols = validate_symbols(symbols, no_data_syms)
    sym_count = len(valid_symbols)
    # TODO: make tests for the below
    # ['AHPI']: no analyst data
    # ['FOUR']: no analyst data
    # ['FOUR']: no target price data
    i = 0
    c = 1
    while i < sym_count:
        if (i + c) < sym_count:
            end = i + c
        else:
            end = sym_count

        class IEX:
            GET_NEW_DATA = True
            GET_REAL_PAID_DATA = True

        _ = get_iex_df(valid_symbols[i:end], IEX())
        i += c
        print(i)
