# Type python m.py to run file
# TODO: add option (old 1 time run) nested option
#   that includes install requirements and set up virtual env
# TODO: add list of useful commands (maybe more menu options)
# TODO: would be nice to just specify the name of the test in pytest, and have it find it and run it
# Can do with os.listdir
# Find matching file, set that as command line arguments
# Ugh I mean we should just have
# https://stackoverflow.com/questions/11472843/set-up-python-on-windows-to-not-type-python-in-cmd
# Couldn't get this to work
"""
C:\ProgramData\Anaconda3\Scripts\activate backend
to test a single file (use -v -s to include print statements)
pytest -v -s /path/to/test_file.py::test_name
pytest -v -s reporting/tests.py::TestHHienReportsView
Run only failed tests
pytest --lf
"""
import subprocess


class MenuOption:
    """
    class to automatically assign number to MenuOption
    Allows for easy rearranging of options or changing of them
    """
    # Declare class/static variables
    option = 1  # Keep count of options in menu
    menu = "Press enter after pressing key\n"  # menu title
    # Set first one to nothing so MenuOptions can start at 1
    options = [None]  # List of instances of MenuOption

    def __init__(self, option_text, output_text, command_line_being_run, subprocess_call):
        """
        :param option_text: Text to display in list of menu options
        :param output_text: Text displayed after option is selected
        :param command_line_being_run: Print command line argument being run
        :param subprocess_call: Subprocess.call variables as list
        """
        # Set instance variables
        self.option_text = str(MenuOption.option) + " - " + option_text
        self.output_text = output_text
        self.command_line_being_run = command_line_being_run
        self.subprocess_call = subprocess_call

        # Set class variables
        MenuOption.option += 1
        MenuOption.options.append(self)
        # Append option to list of options
        MenuOption.menu += self.option_text + "\n"

    def runOption(self):
        """
        When called prints out what command lines are being run
        :return:
        """
        print(self.output_text)
        print(self.command_line_being_run)
        subprocess.call(self.subprocess_call)

    @staticmethod
    def run():
        """
        Creates menu for user to select from
        Given user input, runs that option
        """
        option = int(input(MenuOption.menu))
        MenuOption.options[option].runOption()


MenuOption("Get latest, run scraper, send email", "Get latest, run scraper, send email", "python do_all.py",
           ["python", "do_all.py"])
MenuOption("Run main", "Running analyzer, analysis scraper, iex", "python main.py", ["python", "main.py"])
# do_all
MenuOption("Run IEX script", "running iex script", "python iex/iex_script.py", ["python", "iex/iex_script.py"])

MenuOption("Getting latest IEX Analyst Ratings", "running iex/get_analyst_data", "python iex/get_analyst_data.py",
           ["python", "iex/get_analyst_data.py"])

MenuOption("Tests - Run", "running all tests and generating html report",
           "pytest --cov --cov-report html -ra --tb=line -v",
           ["pytest", "--cov", "--cov-report", "html", '-ra', '--tb=line', '-v'])

MenuOption("Tests - only tests that just failed", "running last failed tests", "pytest --lf -v -s",
           ["pytest", "--lf", '-v', '-s'])

MenuOption.run()
