import logging
import os
from datetime import timedelta

import pandas as pd

from iex.api import API

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

# Batch Requests: Currently only available for /stock endpoints
REAL = False
GET_NEW_DATA = True
if REAL:
    base_url = 'https://cloud.iexapis.com/stable'
    token_str = f'token={os.environ.get("IEX REAL TOKEN")}'
else:
    base_url = 'https://sandbox.iexapis.com/stable'
    token_str = f'token={os.environ.get("IEX TEST TOKEN")}'

# https://iexcloud.io/docs/api/#api-reference
symbol = ['msft', 'aapl']
expiry_date = '202012'
# r = get_options(symbol, expiry_date)
# data = r.json()
# logger.info(r.json())
# with open(f'{symbol}.json', 'w') as f:
#     f.write(json.dumps(data))

# So get latest price of stock symbols, and analysts recommendations, write to excel sheet
use_real_paid_for_data = False
api = API(use_real_paid_for_data)
days = 1


# r = api.get_quote(symbol)
# data = r.json()
# logger.debug(r.json())
# with open(f'{symbol}-quote.json', 'w') as f:
#     import json
#
#     f.write(json.dumps(data))
#
# df = api.get_analyst_recommendations_and_return_df(symbol)
# df.to_csv(f'{symbol}-analyst-recommendation.csv')\
# filename = '../tables/files/s&p500_ticker_symbols.txt'
# filename = 'tables/files/symbols.txt'
# with open(filename, 'r') as f:
#     all_symbols = [line.strip('\n') for line in f]
# all_symbols = [w.replace('.TO', '-CT') for w in all_symbols]
####################
def get_MA_old_not_used():
    range = '1y'
    # ytd
    num_symbols = len(all_symbols)
    i = 0
    df = pd.DataFrame()
    if GET_NEW_DATA:
        while i < num_symbols:
            if i + 40 > num_symbols:
                end = num_symbols
            else:
                end = i + 40
            symbols = all_symbols[i:end]
            df_t = api.get_historical_prices(symbols, range)
            df = df.append(df_t)
            logger.info(f'finished batch {end}')
            i += 40
        df.to_csv('iex_historical_data.csv')
    else:
        df = pd.read_csv('iex_historical_data.csv')
    # Get 50 day MA on specific date
    df = df[['date', 'symbol', 'close']]
    df = df.set_index('date')
    df['close'] = df['close'].astype(float)
    groups = df.groupby('symbol')
    past_days = 50
    df.index = pd.to_datetime(df.index)
    df_og = df.copy()
    max_date = dt2(2020, 1, 1)
    min_date = max_date - timedelta(past_days)
    df = df[(df.index > min_date)]
    df2 = pd.DataFrame(all_symbols, columns=['symbol'])
    # latest_MA = f'{dt2.now().strftime("%Y-%m-%d")} 50 MA'
    dec_31_MA = '2019-12-31 50 MA'
    latest_MA = 'latest 50 MA'
    df2[latest_MA] = None
    df2['current price'] = None
    for symbol in all_symbols:
        # df[f'{symbol} {past_days} MA'] = df[df['symbol'] == symbol.upper()].rolling(window=past_days, min_periods=1)[
        #     'close'].mean()
        val = df[df['symbol'] == symbol.upper()].rolling(window=past_days, min_periods=1)['close'].mean()
        if val.empty:
            continue
        try:
            df2.loc[df2['symbol'] == symbol, dec_31_MA] = val[val.index == dt2(2019, 12, 31)].values[0]
        except IndexError:
            pass
        df2.loc[df2['symbol'] == symbol, latest_MA] = val.tail(1).values[0]
        df2.loc[df2['symbol'] == symbol, 'current price'] = df[df['symbol'] == symbol.upper()].tail(1)['close'].values[
            0]

        # Lets just get this and the current price for now, am tired  # Here, just want to get the last value, well, should get the MA for today as well  # Want the index to be the symbol  # Then
    df2['50 MA inc'] = df2[dec_31_MA] / df2[latest_MA]
    df2['latest inc'] = df2[dec_31_MA] / df2['current price']

    def remove_symbols_from_df(df, symbols_list):
        for symbol in symbols_list:
            df = df.drop(df[df['symbol'] == symbol].index)
        return df

    all_symbols_to_drop = ['LK', 'AMRN']
    df2 = remove_symbols_from_df(df2, all_symbols_to_drop)

    df2.to_csv('iex_data.csv', index=False)  #### Get 50 day MA on Dec 31st #####


# https://sandbox.iexapis.com/stable/stock/market/batch?symbols=AAPL,MSFT&types=chart&chartCloseOnly=True&range=5d&token=mytoken
# If I do 1 day it has a different format?
df = api.get_historical_prices(['AAPL', 'MSFT'], '1d')
