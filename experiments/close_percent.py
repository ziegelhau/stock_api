import pandas as pd

df = pd.read_excel('ixic 5 year.xlsx')
# df = pd.read_excel('IXIC.xlsx')
# df = df[['Close %', '% change today', '% change tomorrow']].astype(float)
p = df['% change today'] > 0
c = p.cumsum()
df['days up in a row'] = c - c.mask(p).ffill().fillna(0).astype(int)
df['close < 50%'] = df['Close %'] < 0.5

corr = df[['close < 50%', 'Close %', 'days up in a row', '% change tomorrow', 'down tomorrow']].corr()
# corr.style.background_gradient(cmap='coolwarm').set_precision(2)
import matplotlib.pyplot as plt
# size = 10
# fig, ax = plt.subplots(figsize=(size, size))
# ax.matshow(corr)
# plt.xticks(range(len(corr.columns)), corr.columns);
# plt.yticks(range(len(corr.columns)), corr.columns);
# import matplotlib.pyplot as plt
#
# plt.matshow(corr)
# # 'RdBu_r' & 'BrBG' are other good diverging colormaps
# # [pearson_coeff, p_value] = pearsonr(df['Close %'].astype(float), df['tomorrows close'].astype(  #     float))  # returs pearson coefficient and 2 tailed p     # value


"""
I want to know if tomorrow is down on X axis, days up in a row on Y axis?
"""  # hist(df['down tomorrow'], df['days up in a row'])#.hist(by='down tomorrow')  # plot(df['days up in a row'], df['down tomorrow'])  # Want to get the true and false count for each days up in a row  # g = df.groupby('days up in a row')  # a = g['down tomorrow'].sum() / g['down tomorrow'].count()
