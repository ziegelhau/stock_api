import logging
import os

import pandas as pd
from pandas._testing import assert_frame_equal

from misc.utils import create_or_update_csv_file

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)


def del_file_if_it_exists(filename):
    try:
        os.remove(filename)
    except OSError:
        pass


class Test_create_or_update_csv_file:
    def setup_class(self):
        self.fn = 'temp.csv'
        self.df = pd.DataFrame({'col1': ['a', 'b', 'c'], 'col2': [1, 2, 3]})
        self.df2 = pd.DataFrame({'col1': ['a', 'b', 'c', 'd'], 'col2': [1, 2, 3, 4]})

    def test_create_file_if_it_doesnt_exist(self):
        del_file_if_it_exists(self.fn)
        create_or_update_csv_file(self.df, self.fn)
        df = pd.read_csv(self.fn, index_col=False)
        assert_frame_equal(df, self.df)

    def test_do_nothing_if_data_already_in_file(self):
        self.df.to_csv(self.fn, index=False)
        df = pd.DataFrame({'col1': ['a'], 'col2': [1]})
        create_or_update_csv_file(df, self.fn)
        df = pd.read_csv(self.fn, index_col=False)
        assert_frame_equal(df, self.df)

    def test_append_entries(self):
        self.df.to_csv(self.fn, index=False)
        df = pd.DataFrame({'col1': ['d'], 'col2': [4]})
        create_or_update_csv_file(df, self.fn)
        df = pd.read_csv(self.fn, index_col=False)
        assert_frame_equal(df, self.df2)

    def test_append_only_new_entries(self):
        # Column is also added in this test
        self.df.to_csv(self.fn, index=False)
        create_or_update_csv_file(self.df2, self.fn)
        df = pd.read_csv(self.fn, index_col=False)
        assert_frame_equal(df, self.df2)

    def test_del_col(self):
        pass

    def teardown_class(self):
        del_file_if_it_exists(self.fn)
