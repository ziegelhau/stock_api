Composite Rating Components
    Earnings Per Share (EPS) Rating - More Weight
    Relative Price Strength (RS) Rating - More Weight
    Industry Group Relative Strength Rating
        A measurement of a stock's industry group performance over the past six months utilizing an A+ to E scale.
    Sales+Profit Margins+ROE (SMR) Rating
        Sales growth rate over the last three quarters;
    Accumulation/Distribution Rating
        it tracks the relative degree of institutional buying (accumulation) and selling (distribution) in a particular stock over the last 13 weeks.
    % Off 52 week high

Composite Rating Components
    Earnings Per Share (EPS) Rating - More Weight
    Relative Price Strength (RS) Rating - More Weight
    Industry Group Relative Strength Rating
        A measurement of a stock's industry group performance over the past six months utilizing an A+ to E scale.
    Sales+Profit Margins+ROE (SMR) Rating
    Sales growth rate over the last three quarters;
        Pre-tax profit margins;
        After-tax profit margins;
        Return on equity (ROE)
        Sales growth and after-tax margins are computed with quarterly figures, while return on equity and pre-tax margins are calculated using annual figures. All four factors take into account acceleration of the variables (rate of increase).
        The rating system is weighted in the following manner:
        A = Top 20% (Outperforming over 80% of other stocks)
        B = Next 20% (Outperforming 60%-80% of other stocks)
        C = Next 20% (Outperforming 40%-60% of other stocks)
        D = Next 20% (Outperforming 20%-40% of other stocks)
        E = Bottom 20% (Outperformed by over 80% of other stocks)
        NOTE: Stocks will be listed as "N/A" when some or all of the variables used for calculation, are not available.
    Accumulation/Distribution Rating
        it tracks the relative degree of institutional buying (accumulation) and selling (distribution) in a particular stock over the last 13 weeks.
        A = Heavy buying
        B = Moderate buying
        C = Equal amount of buying and selling
        D = Moderate selling
        E = Heavy selling
    % Off 52 week high