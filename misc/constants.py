class Metric:
    ad_rating = 'a/d rating'
    ad_rating_int = 'a/d rating int'
    comp_rating = 'comp rating'
    cur_price = 'current price'
    eps_rating = 'eps rating'
    eps_last_reported = 'eps lst rptd'
    eps_due_date = 'eps due date'
    eps_surprise = 'eps surprise'
    eps_per_chg_lst_qtr = 'eps % chg last qtr (-/+)'
    ind_group_rank = 'ind group rank'
    num_funds = 'number of funds'
    per_off_high = '% off high'  # want to add in a description
    rs_rating = 'rs rating'
    sales_per_chg_lst_qtr = 'sales % chg lst qtr'
    up_down_vol = 'up/down vol'
    vol_50_day_avg = '50-day avg vol (1000s)'
    vol_per_chg_50_day = 'vol % chg vs 50-day'
    days_since_pivot = 'days since pivot'
    pivot_pct_away = 'pivot percent away'
    ind_name = 'industry name'
    sector = 'sector'
    rs_line_new_high = 'rs line new high'
    price_vs_21_day = 'price vs 21-day'
    price_vs_50_day = 'price vs 50-day'
    days_til_earnings = 'days until earnings'
    pct_chg_6_m = '% chg 6 months'
    pct_chg_12_m = '% chg 12 months'
    symbol = 'symbol'
    name = 'name'


# class metric:  #     def __init__(self, name, descr=none):
#         self.name = name
#         if descr is none:
#             self.descr = none
#         else:
#             self.descr = descr
#
#
# per_off_high = metric('% off high', "relative to 52-week high")

# class marketsmithscraper:

DATETIME_STR_FORMAT = "%Y-%m-%d %H:%M"

# 'a/d rating a,b,c', is a pass
# Not sure if this scaling and replacement is accurate
LET_TO_NUM = {'A+': 10, 'A': 9, 'A-': 8, 'B+': 7, 'B': 6, 'B-': 5, 'C+': 4, 'C': 3, 'C-': 2, 'D+': 1, 'D': 0, 'D-': -1,
              'E+': -2, 'E': -3, 'E-': -4}

ONEIL_COLS = ['eps rating ≥ 80', 'rs rating ≥ 80', 'a/d rating a,b,c', 'eps % chg last qtr (-/+) ≥ 20',
              'sales % chg lst qtr ≥ 20', 'current price ≥ 10', '50-day avg vol (1000s) ≥ 100', 'number of funds ≥ 5',
              '% off high ≥ -15']
ARNIE_COLS = ['rs rating ≥ 85', 'comp rating ≥ 90',
              'up/down vol ≥ 1.1']  # ,'ind group rank ≤ 20', 'vol % chg vs 50-day ≥ 20', 'rs line new high']
ARNIE_CRITERIA = 1  # (5/5)
ONEIL_COL_COUNT = len(ONEIL_COLS)
ONEIL_CRITERIA = (ONEIL_COL_COUNT - 2) / ONEIL_COL_COUNT  # 7/9
BASIC_COLS = ['name', 'total score', 'analysis', 'john owns']
