import logging

import pandas as pd
from pandas._testing import assert_frame_equal
from pandas.errors import EmptyDataError

from misc.constants import BASIC_COLS

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)


def df_is_equal(df1, df2):
    try:
        assert_frame_equal(df1, df2, check_less_precise=True)
        return True
    except AssertionError:
        return False


def create_latest_csv(all_filename, recent_filename, sep=','):
    # Get most recent data and write to separate file
    df = pd.read_csv(all_filename, sep=sep, index_col=False)
    # TODO: get latest not null values
    df = get_latest_df(df)
    df.to_csv(recent_filename, sep=sep, index=False)


def get_latest_df(df):
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df = df.sort_values('timestamp').groupby('symbol').tail(1)
    df = df.loc[df.groupby('symbol')['timestamp'].idxmax()]
    return df


def create_or_update_csv_file(df, filename, write_index=False):
    try:
        # If the df has not been added to the history file yet, do it
        # Improvement history_df = pd.read_csv(Fname.all_history.value, dtype={'timestamp': dt2 }))
        try:
            history_df = pd.read_csv(filename)
        except FileNotFoundError:
            open(filename, 'a').close()
            history_df = pd.DataFrame()
            df.to_csv(filename, index=write_index)
            return
        if 'timestamp' in history_df.columns:
            history_df['timestamp'] = pd.to_datetime(history_df['timestamp'])
        if 'Date' in history_df.columns:
            history_df['Date'] = pd.to_datetime(history_df['Date'])
        # Need to find columns that are different between the dfs
        if history_df.columns.to_list() != df.columns.to_list():
            # New columns will get added automatically, need to add code to delete columns we don't want anymore
            to_del = []
            for item in history_df.columns.to_list():
                if item not in df.columns.to_list():
                    to_del.append(item)
            if to_del:
                if len(to_del) == 1:
                    history_df = history_df.drop(to_del, axis=1)
                else:
                    history_df = history_df.drop([to_del], axis=1)
        df_all = history_df.merge(df, on=history_df.columns.tolist(), how='outer', indicator=True)
        df_all = df_all.drop('_merge', axis=1)
        df_all.to_csv(filename,
                      index=write_index)  # df_new = df_all[df_all['_merge'] == 'left_only']  # if not df_new.empty:  #     df_new = df_new.drop('_merge', axis=1)  #     df_new.to_csv(history_file, mode='a', index=False, header=False)
    # If the history file doesn't exist yet
    except EmptyDataError:
        df.to_csv(filename, index=write_index)


class User:
    s = []

    def __init__(self, email, columns=None):
        User.s.append(self)
        self.email = email
        if columns is None:
            self.columns = BASIC_COLS
        else:
            self.columns = columns
