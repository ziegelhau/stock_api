import logging
import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO

from pandas import ExcelWriter

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)


def send_email(df_all, user):
    df = df_all[user.columns]
    logger.info(f'Emailing {user.email}')
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    msg = MIMEMultipart()
    msg['Subject'] = f'Stock Analysis'
    msg['From'] = EMAIL_HOST_USER
    # msg['To'] = EMAIL_HOST_USER
    emails_to_send_to = user.email  # + [EMAIL_HOST_USER] # do this to BCC multiple people
    df1 = create_excel_attachment_given_df(df)
    # TODO: make this a function send_email
    # msg.attach(MIMEText(f"Top 50. Excel sheet shows all {len(df)} "))
    msg.attach(MIMEText(df.head(50).to_html(), 'html'))
    part = MIMEBase('application', "octet-stream")
    part.set_payload(df1)
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="buy_df.xlsx"')
    msg.attach(part)
    msg.attach(MIMEText(
        "\nDisclaimer: The above references an opinion and is for information purposes only. It is not intended to be "
        "investment advice. Seek a duly licensed professional for investment advice"))
    s = smtplib.SMTP(EMAIL_HOST, 587)
    s.starttls()
    s.login(EMAIL_HOST_USER, EMAIL_HOST_PASSWORD)
    logger.debug("start sending email")
    s.sendmail(EMAIL_HOST_USER, emails_to_send_to, msg.as_string())
    s.quit()
    logger.debug("sent email")


def create_excel_attachment_given_df(df, multiple_sheets=None):
    """
    param df: Pandas df
    multiple_sheets: Parameter to group df by and write to each sheet
    """

    bio = BytesIO()
    writer = ExcelWriter(bio, engine='xlsxwriter')
    if multiple_sheets is not None:
        groups = df.groupby(multiple_sheets)
        for group in groups:
            group[1].to_excel(writer, sheet_name=group[0])
    else:
        df.to_excel(writer)
    writer.save()
    bio.seek(0)
    # get the excel file
    workbook = bio.read()
    return workbook
