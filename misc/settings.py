########## Inputs ##########
from misc.constants import Metric as M, BASIC_COLS
from misc.utils import User

RUN_WEBSCRAPER = False
EMAIL_JAMES = False
GET_LATEST_MARKETSMITH_FILE = False
TIME_STOCK_ON_SCREEN = 0.0
# EMAIL_LIST = ['mikoman@telus.net', 'jamesjrobertson42@gmail.com', 'katysemcow@gmail.com', 'siraj.farage@gmail.com',
#               'johnmiko4@gmail.com']
# EMAIL_LIST = ['mikoman@telus.net', 'jamesjrobertson42@gmail.com', 'katysemcow@gmail.com', 'siraj.farage@gmail.com',
#               'johnmiko4@gmail.com']
EMAIL_LIST = ['johnmiko4@gmail.com', 'johnwmiko@gmail.com', 'johnmiko4spam@gmail.com']
NO_IEX_DATA = ['CMCL', 'OBCI', 'EDUC', 'RVP']
OTHER_HOLDINGS = ['FNV', 'RMD', 'DXCM', 'AMED']


class IEX:
    GET_NEW_DATA = False
    GET_REAL_PAID_DATA = False


class Folder:
    downloads = 'C:/Users/johnm/Downloads/'
    tables = 'tables'


class Fname:
    pre_buy = 'pre_buy.csv'
    buy = 'buy.csv'
    iex_out = f'{Folder.tables}/iex analysts.csv'
    marketsmith_all = f'{Folder.tables}/marketsmith all.csv'
    marketsmith_recent = f'{Folder.tables}/marketsmith current.csv'
    td_in = '-holdings-'
    td_out = 'td.csv'
    all_current = f'{Folder.tables}/all current.csv'
    all_history = f'{Folder.tables}/all history.csv'


########## Inputs ##########
DEFAULT_LOAD_TIME = 5  # Time to wait for page/element to load before throwing error
MIN_RS_RATING = 82
ANALYSIS_COLS = [M.symbol, M.name, 'total score', 'sector', 'analysis', M.up_down_vol, M.rs_rating,
                 M.comp_rating, M.ind_group_rank, M.pivot_pct_away, M.days_since_pivot, M.cur_price, 'rs rating scaled']
GOOD_SECTORS = ['CHIPS', 'SOFTWARE', 'TELECOM', 'BUSINS SVC', 'INTERNET', 'ENERGY', 'BUILDING', 'MEDICAL']  # not used
GOOD_INDUSTRIES = ['Internet', 'Computer']  # not used
OUTPUT_COLS = ['symbol', 'name', 'total score', 'analysis', 'days until earnings', 'days since earnings',
               M.eps_surprise]
ALL_COLS = ['name', 'total score', M.cur_price, 'analysis', 'sector', 'days until earnings',
            'days since earnings', M.eps_surprise, 'john owns']
HOLDING_COLS = ['symbol', 'strike price', 'days until expiry', 'quantity', 'book cost']
CSV_FILE_FOR_ANALYSIS = ['johns screener']

# User('johnwmiko@gmail.com', BASIC_COLS)
# User('johnmiko4spam@gmail.com', ALL_COLS)

all_users = [['mikoman@telus.net', BASIC_COLS + ['sector']],
             'jamesjrobertson42@gmail.com', ALL_COLS,
             'katysemcow@gmail.com', BASIC_COLS,
             'siraj.farage@gmail.com', ALL_COLS,
             'johnmiko4@gmail.com', ALL_COLS]
all_users = [['johnwmiko@gmail.com', BASIC_COLS], ['johnmiko4spam@gmail.com', ALL_COLS]]
for user in all_users:
    User(user[0], user[1])

# User('mikoman@telus.net', BASIC_COLS + ['sector'])
# User('jamesjrobertson42@gmail.com', ALL_COLS)
# User('katysemcow@gmail.com', BASIC_COLS)
# User('siraj.farage@gmail.com', ALL_COLS)
