import datetime

import pandas as pd
import pandas_datareader.data as web

# end = datetime.datetime(2020, 7, 20)
# start = datetime.datetime(2020, 7, 17)
from misc.utils import create_or_update_csv_file

end = datetime.datetime(2020, 7, 16)
start = end - datetime.timedelta(days=50)
with open('../../tables/files/s&p500_companies_by_market_cap.txt', 'r') as f:
    symbols = [word.strip() for word in f]
df = pd.read_csv('../../all_buy.csv', index_col=False)
symbols = df['symbol'].to_list()
db = pd.DataFrame()
c = 0
len_sym = len(symbols)
with open('../../iex/errors.csv', 'w') as f:
    for symbol in symbols:
        c += 1
        print(f'{c}:{len_sym} {symbol}')
        try:
            df = web.DataReader(symbol, 'yahoo', start, end, pause=0.2)
        except:
            try:
                df = web.DataReader(symbol, 'yahoo', start, end, pause=0.2)
            except:
                print(f'Did not get data for {symbol}')
                f.write(f'{symbol}\n')
        df = df.rename(columns={'Close': 'close'})
        df['close'] = df['close'].round(2)
        df['symbol'] = symbol
        create_or_update_csv_file(df[['symbol', 'close']], '../../tables/close_price.csv')
