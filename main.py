import logging
import os
from datetime import datetime as dt2

import numpy as np
import pandas as pd

from analysis.screeners import BuyScreener
from analysis.utils import Score, get_df, Holdings, join_list, add_columns_to_df
from menu.get_analyst_data import get_analyst_data
from misc.send_email import send_email
from misc.settings import CSV_FILE_FOR_ANALYSIS, Folder, Fname, OUTPUT_COLS, ANALYSIS_COLS, TIME_STOCK_ON_SCREEN, \
    NO_IEX_DATA
from misc.utils import df_is_equal, create_latest_csv, create_or_update_csv_file, User
from scraper.kill_drivers.kill_drivers import kill_drivers
from scraper.marketsmith_scraper import scraper, download_latest_marketsmith_list

kill_drivers()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)


def main(settings):
    if settings.GET_LATEST_MARKETSMITH_FILE:
        _ = download_latest_marketsmith_list()

    if settings.RUN_WEBSCRAPER:
        # Get pivots for symbols we are thinking about buying
        _ = scraper(symbols_list=pd.read_csv(Fname.buy)['symbol'].to_list(),
                    time_stock_on_screen=TIME_STOCK_ON_SCREEN)

    create_latest_csv(Fname.marketsmith_all, Fname.marketsmith_recent)
    downloads_folder_files = os.listdir(Folder.downloads)
    df = get_df(downloads_folder_files, CSV_FILE_FOR_ANALYSIS)
    create_or_update_csv_file(df, Fname.all_history)
    df = add_columns_to_df(df)
    td_df = Holdings.get_df(downloads_folder_files, df)
    get_analyst_data(NO_IEX_DATA)
    iex_df = pd.read_csv(Fname.iex_out)
    iex_df['ratingscalemark'] = iex_df['ratingscalemark'].fillna(1.5)
    df = df.merge(iex_df, on='symbol', how='left')
    df['analysis valid for'] = (dt2.today() - pd.to_datetime(df['updateddate'])).dt.days
    df['analysis valid for'] = df['analysis valid for'].fillna(1000)
    df['analysis'] = np.empty((len(df), 0)).tolist()
    df['cup'] = 0
    score_inst = Score()
    df = df.apply(score_inst.cup, axis=1)
    df = df.apply(score_inst.analysts, axis=1)
    scores = score_inst.s
    for score in scores:
        df = getattr(score_inst, score)(df)
    df['analysis'] = df['analysis'].apply(join_list)
    df = score_inst.total(df)
    df = df.sort_values('total score', ascending=False)
    df.to_csv(Fname.pre_buy, index=False)
    screeners = [BuyScreener]  # , OneilScreener]
    buy_df = df.copy()
    for screener in screeners:
        buy_df = screener.apply(buy_df)
    buy_df = buy_df.round(2)
    buy_df[OUTPUT_COLS].to_csv(Fname.buy, index=False)
    old_buy_df = pd.read_csv(Fname.pre_buy)
    df[ANALYSIS_COLS].to_csv(Fname.all_current, index=False)

    if (not df_is_equal(old_buy_df[['symbol', 'total score']], df[['symbol', 'total score']])) and settings.EMAIL_JAMES:
        buy_df['john owns'] = buy_df['symbol'].isin(td_df['symbol']) | buy_df['symbol'].isin(
            settings.OTHER_HOLDINGS)
        buy_df['john owns'] = buy_df['john owns'].replace(False, '')
        email_df = buy_df.copy()
        email_df = email_df.set_index('symbol')
        for user in User.s:
            send_email(email_df, user)
    return df

# df = main(settings)
