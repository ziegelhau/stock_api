from datetime import datetime as dt2

from main import main
from misc import settings

settings.RUN_WEBSCRAPER = False
settings.EMAIL_JAMES = False
settings.GET_LATEST_MARKETSMITH_FILE = False


def do_all():
    # while True:
    df = main(settings)
    print(f'ran at {dt2.now()}')  # time.sleep(3600)  # run every hour
    return df


df = do_all()
