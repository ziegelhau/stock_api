import logging

import pandas as pd

from misc.constants import Metric as M

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

from analysis.utils import Score, create_safe_sector_column


class TestScore:
    score = Score()
    buy_analy = {'buy': 0, 'analysis': [], M.up_down_vol: 5, M.rs_rating: 100}

    def test_all_return_types_are_correct(self):
        pass

    def test_arnie_criteria_false_no_buy(self):
        in_dict = {'oneil_criteria': True, 'arnie_criteria': False, 'pivot percent away': -0.05, 'days since pivot': -1,
                   **self.buy_analy}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict

    def test_oneil_criteria_false_no_buy(self):
        in_dict = {'oneil_criteria': False, 'arnie_criteria': True, 'pivot percent away': -0.05, 'days since pivot': -1,
                   **self.buy_analy}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict

    def test_pivot_too_high_no_buy(self):
        in_dict = {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': 2, 'days since pivot': -1,
                   **self.buy_analy}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict

    def test_days_since_pivot_between_0_and_13_days_no_buy(self):
        in_dict = {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': 0.01, 'days since pivot': 0,
                   **self.buy_analy}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict
        in_dict['days since pivot'] = 13
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict
        in_dict['days since pivot'] = 13

    def test_below_pivot_is_a_buy(self):
        # TODO: need to check that this is actually correct
        in_dict = {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': -0.05, 'days since pivot': -1,
                   'buy': 0, 'analysis': [], M.rs_rating: 100, M.up_down_vol: 5}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == {'rs rating': 100, 'up/down vol': 5, 'oneil_criteria': True, 'arnie_criteria': True,
                          'pivot percent away': -0.05, 'days since pivot': -1, 'buy': 7,
                          'analysis': ['very strong company. below pivot by 5%']}

    def test_2_weeks_after_pivot_is_a_buy(self):
        in_dict = {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': -0.05, 'days since pivot': 14,
                   'buy': 0, 'analysis': []}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': -0.05,
                          'days since pivot': 14, 'buy': 5, 'analysis': ['pivot: -5% away, 2 weeks ago']}

    def test_5_days_after_pivot_is_not_a_buy(self):
        in_dict = {'oneil_criteria': True, 'arnie_criteria': True, 'pivot percent away': -0.05, 'days since pivot': 5,
                   **self.buy_analy}
        output = self.score.cup(in_dict)
        logger.debug(output)
        assert output == in_dict


class TestScorePostEarnings:
    score = Score()

    def test_1(self):
        in_dict = {'eps due date': '10-29-2020', 'eps surprise': 50.0}
        # in_dict = {'eps due date': True, 'arnie_criteria': True, 'pivot percent away': -0.05,
        #            'days since pivot': 5,
        #            **self.buy_analy}

        output = self.score.post_earnings(pd.DataFrame([in_dict]))
        # output = self.score(in_dict)
        logger.debug(output)
        assert output == in_dict


class TestSafeSector:
    def test_simple(self):
        df = pd.DataFrame({'symbol': ['a', 'b', 'c', 'd'], 'sector': ['Safe sector', 'Safe Sector', 'Nope', 'Nope'],
                           "industry name": ['Safe Indy', 'Nope', 'Also a Safe Indy', 'Nope']})
        df_new = create_safe_sector_column(df, good_sectors=['Safe Sector'], good_industries=['Safe Indy'])
        assert df_new['safe business'].to_list() == [True, True, True, False]

    def test_realistic(self):
        df = pd.DataFrame({'symbol': ['a', 'b', 'c', 'd'], 'Sector': ['Nope', 'Nope', 'Nope', 'Nope'],
                           "industry name": ['Computer Sftwr-Enterprse', ' Medical-Biomed/Biotech', 'Internet-Content',
                                             'Medical-Supplies']})
        df_new = create_safe_sector_column(df, good_sectors=['Safe Sector'], good_industries=['Internet', 'Computer'])
        assert df_new['safe business'].to_list() == [True, False, True, False]
