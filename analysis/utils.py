import logging
import os
import time
from datetime import datetime as dt2
from datetime import timedelta

import pandas as pd
from pandas.errors import EmptyDataError

from iex.api import API
from misc.constants import DATETIME_STR_FORMAT, LET_TO_NUM
from misc.constants import Metric as M
from misc.settings import Fname, Folder, HOLDING_COLS

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

from scipy.interpolate import interp1d


class MultiStep:
    def __init__(self, maps: list, min=None, new_min=0, max=None, new_max=1, limits_first=False, new_col_name='scaled'):
        self.maps = maps
        self.min = min
        self.new_min = new_min
        self.max = max
        self.new_max = new_max
        self.limits_first = limits_first
        self.new_col_name = new_col_name

    def apply(self, df, cols):
        """
        Only works for a single column right now
        :param df:
        :param cols:
        :return:
        """
        new_col = f'{cols} {self.new_col_name}'
        df[new_col] = df[cols].copy()
        df[new_col] = df[new_col].astype(float)
        if self.limits_first:
            if self.min is not None:
                df.loc[df[new_col] < self.min, new_col] = self.new_min
            if self.max is not None:
                df.loc[df[new_col] > self.max, new_col] = self.new_max
            for map in self.maps:
                df = map.apply(df, new_col)
        else:
            for map in self.maps:
                df = map.apply(df, new_col)
            if self.min is not None:
                df.loc[df[new_col] < self.min, new_col] = self.new_min
            if self.max is not None:
                df.loc[df[new_col] > self.max, new_col] = self.new_max
        return df


class Map:
    def __init__(self, l, h, nl, nh):
        # Improvement: default low = column name min, default high = column max
        # default scaling from 0-1
        self.l = l
        self.h = h
        self.nl = nl
        self.nh = nh

    def apply(self, df, cols):
        # df[cols] = df[cols].copy()
        m = interp1d([self.l, self.h], [self.nl, self.nh])
        rows = (df[cols] >= self.l) & (df[cols] <= self.h)
        df.loc[rows, cols] = m(df.loc[rows, cols])
        return df


class Criteria:
    s = []

    def __init__(self, name, col, mid, operator, mn=None, mx=None, scaler=None, weight=1):
        # Criteria.instances[name] = self
        Criteria.s.append(self)
        self.name = name
        self.col = col
        self.min = mn
        self.max = mx
        self.mid = mid
        self.operator = operator
        self.scaler = scaler
        self.weight = weight

    def apply(self, series, formula):
        logger.debug(self.name)
        # Scale the value to the min and the max? ugh Id on't know just going to have to code
        if formula == 'step':
            return self.operator(series, self.mid).astype(int)
        elif formula == 'multi-step':
            m = interp1d([1, 512], [5, 10])
            m(256)
        else:
            raise NotImplementedError()


class Score:
    @property
    def s(self):
        exceptions = ['s', 'total', 'cup', 'analysts']
        public_method_names = [method for method in dir(Score) if callable(getattr(Score, method)) if
                               ((not method.startswith('_')) and (method not in exceptions))]
        return public_method_names

    def pivot_percent_away(self, df):
        df['pivot percent away inverse'] = -1 * df['pivot percent away']
        df = MultiStep([Map(-0.1, 0.1, 0, 1)], min=-0.1, new_min=-0.1, max=0.1, new_max=0.1, limits_first=True).apply(
            df, 'pivot percent away inverse')
        df = df.rename(columns={'pivot percent away inverse scaled': 'pivot score'})
        return df

    def cup(self, row):
        """
        Improvements:
            Should include the % away from pivot in here, get weird combinations happening
            Sell if
                days since pivot > 21 and pivot percent away > 0.2
                or
                days since pivo > 8 weeks (56 days)

            Include rs line new high
            Change RS Line to new High to be rs line new high in past 3 trade days (so ex Friday, Monday, Tuesday)
            Include 'vol % chg vs 50-day ≥ 20'
            Calculate slope since breakout day and check how big it is
            if doesn't break out sell after week
            days since pivot should max out at 5 weeks I think, start to decrease score after that
        :param row:
        :return:
        """
        if row['pivot percent away'] < 0.12:
            # Criteria('temp', M.rs_rating, 79, operator.gt)
            # temp = Score(7, Criteria('rs rating >= 90', M.rs_rating, 90, Op.gte),
            #              Criteria('days since pivot < 0', M.days since pivot, 0, Op.lt),
            #              Criteria('up/down volume >= 1.2', M.up_down_vol, 0, Op.gte)).calculate(row)
            if row['days since pivot'] < 0:
                row['cup'] = 0.5
                row["days since pivot"] = -7
            elif row['days since pivot'] < 10:
                row['cup'] = 1
            elif row['days since pivot'] > 35:
                row['cup'] = 1
            elif row['days since pivot'] > 21:
                row['cup'] = 2
            else:
                row['cup'] = 2 * row['days since pivot'] / 21
            pivot_percent_away = int(100 * row["pivot percent away"])
            row['analysis'].append(
                f'pivot: {int(pivot_percent_away)}% away, {int(round(row["days since pivot"] / 7, 0))} weeks ago')
        else:
            row['cup'] = -5.5
        return row

    def total(self, df):
        df = self.rs_line_new_high(df)
        df['total score'] = 5.5 * df['cup'] + 17 * df['rs rating scaled'] + 13 * df['rs line new high score'] + 3 * df[
            'pivot score'] + 1 * df[f'{M.pct_chg_12_m} score'] + 0.08 * df[f'{M.per_off_high} score'] + \
                            2 * df['analyst score'] + 1 * df[f'{M.price_vs_21_day} score'] + \
                            3 * df[f'{M.cur_price} score'] + 3 * df['earnings score'] + 1 * df[
                                'days until earnings score']
        # / df['ratingscalemark']
        # df = Map(df['total score'].min(), df['total score'].max(), 0, 1).apply(df, 'total score')
        df['total score'] = round(df['total score'] / 55 * 100, 0)  # actually out of 62 I think
        return df

    def rs_line_new_high(self, df):
        df['rs line new high score'] = df['rs line new high']
        return df

    def post_earnings(self, df):
        df['earnings score'] = 0
        df.loc[df['days since earnings'] <= 10, 'earnings score'] = 1
        # Think the eps_surprise isn't correct
        # df.loc[(df['days since earnings'] <= 7) & (df[M.eps_surprise] > 25), 'earnings score'] = 1
        # df.loc[mask, 'earnings analysis'] = f'Earnings {days_since_earnings} ago, {df[M.eps_surprise]}'
        return df

    def days_until_earnings(self, df):
        # df['days until earnings'] = (pd.to_datetime(df['EPS Due Date']) - dt2.now()).dt.days
        df['days until earnings score'] = 0
        df.loc[df['days until earnings'] <= 2, 'days until earnings score'] = 1
        return df

    def rs_rating(self, df):
        df = MultiStep([Map(80, 85, 0, 1), Map(85, 100, 1, 2)], min=80, new_min=0, limits_first=True).apply(df,
                                                                                                            M.rs_rating)
        return df

    def price_vs_21_day(self, df):
        df = MultiStep([Map(0, 2, 1, 1), Map(2, 5, 1, 0)], min=0, new_min=0, max=5, new_max=0).apply(df,
                                                                                                     M.price_vs_21_day)
        df[f'{M.price_vs_21_day} score'] = df[f'{M.price_vs_21_day} scaled']
        return df

    def price_more_than_20(self, df):
        df[f'{M.cur_price} score'] = 0
        df.loc[df[M.cur_price] > 20, f'{M.cur_price} score'] = 1
        # df[f'{M.per_off_high} score'] = df[M.per_off_high]
        return df

    def percent_off_high(self, df):
        df.loc[df[M.per_off_high] > 0, M.per_off_high] = 0
        df[f'{M.per_off_high} score'] = df[M.per_off_high]
        return df

    def pct_chg_12_m(self, df):
        df = MultiStep([Map(0, 100, 0, 1)], min=0, new_min=0, max=100, new_max=100, limits_first=True).apply(df,
                                                                                                             M.pct_chg_12_m)
        df[f'{M.pct_chg_12_m} score'] = df[f'{M.pct_chg_12_m} scaled']
        return df

    def analysts(self, row):
        """
        :param row:
        :return:
        """
        row['analyst score'] = 0
        if (float(row['total_ratings']) >= 5) and (float(row['analysis valid for']) <= 40):
            cur_price = float(row['current price'])
            high = float(row['pricetargethigh'])
            avg = float(row['pricetargetaverage'])
            low = float(row['pricetargetlow'])
            if cur_price >= high:
                # Problem here because if I apply screener to remove price target high, then the data doesn't get
                # updated
                row['analyst score'] = -5
                pct = int(round((cur_price - high) / cur_price * 100, 0))
                row['analysis'].append(f'estimates: {pct}% above high')
            elif cur_price < low:
                pct = int(round((low - cur_price) / cur_price * 100, 0))
                if pct < 100:
                    row['analyst score'] = 2
                    row['analysis'].append(f'estimates: {pct}% below low')
            elif cur_price < float(avg):
                # Need some scaling here with max being 2
                # Formula seemed reasonable, doesn't have to be this
                pct = int(round((avg - cur_price) / cur_price * 100, 0))
                if pct > 20:
                    row['analyst score'] = 2
                else:
                    row['analyst score'] = 1 + (pct / 20)
                row['analysis'].append(f'estimates: {pct}% below avg')
        return row

    # df = MultiStep([Map(1.1, 3, 0, 1)], min=1.1, new_min=0, max=3, limits_first=True).apply(df,


#                                                                                         'up/down vol')
# up_down_vol = Metric('up/down vol', 0.5)
# total_score = Criteria()
# total_score = Score(up_down_vol, )

class CurrentPriceCriteria(Criteria):
    name = 'current price ≥ 10'
    metric = M.cur_price

    def apply(self, df):
        df[self.name] = 0
        df.loc[df[M.cur_price >= 10], self.name] = 1


class Op:
    gt = 'gt'
    gte = 'gte'
    lt = 'lt'
    lte = 'lte'
    eq = 'eq'

    def __call__(self, op, val1, val2, *args, **kwargs):
        if op == self.gt:
            return val1 > val2
        elif op == self.gte:
            return val1 >= val2
        elif op == self.lt:
            return val1 < val2
        elif op == self.lte:
            return val1 <= val2
        elif op == self.eq:
            return val1 == val2
        else:
            raise ValueError(f'Received operation of {op}, did not have implemented')


# TODO: create Criteria name if it's not input
# Also, need to make sure I don't duplicate criterias
# temp = Score(7, Criteria('rs rating >= 90', M.rs_rating, 90, Op.gte),
#              Criteria('days since pivot < 0', M.days since pivot, 0, Op.lt),
#              Criteria('up/down volume >= 1.2', M.up_down_vol, 0, Op.gte))

# pivot percent away = int(100 * row["pivot percent away"])
# Criteria('temp', M.rs_rating, 79, Op.gte)
# if row['days since pivot'] < 0:
#     if row[M.up_down_vol] > 1.2:
#         if row[M.rs_rating] >= 90:
#             row['buy'] = 7  # (+7, M.up_down_vol > 1.2, M.days since pivot)
# operator.gt


def clean_df(df):
    df.columns = df.columns.str.lower()
    if '#' in df.columns:
        df = df.drop("#", axis=1)
    return df


def get_df(downloads_folder_files, csv_file_analysis):
    csv_file = find_latest_files_in_folder(downloads_folder_files, csv_file_analysis)[0]
    file_age = int(
        round((time.mktime(dt2.now().timetuple()) - os.path.getctime(Folder.downloads + csv_file)) / 3600, 0))
    logger.info(f'screener file is {file_age} hours old')
    df = pd.read_csv(Folder.downloads + csv_file)
    df = clean_df(df)
    if 'timestamp' in df.columns:
        # Don't want to ovewrite the file if we've already appended the timestamp
        # TODO: better to remove the seconds from the timestamp as well because of reading in errors
        df['timestamp'] = pd.to_datetime(df['timestamp'])
    else:
        df['timestamp'] = dt2.now().strftime(DATETIME_STR_FORMAT)
        df['timestamp'] = pd.to_datetime(df['timestamp'])
        df.to_csv(Folder.downloads + csv_file, index=False)
    return df


def find_latest_files_in_folder(file_list, market_files):
    if type(market_files) != list:
        market_files = [market_files]
    files_I_want = []
    # Search in the downloads folders because that's where the files get downloaded.
    # Find the one that is the latest version of the file
    for market_file in market_files:
        potential_matches = []
        for file_name in file_list:
            if market_file in file_name:
                potential_matches.append(file_name)
        ints = []
        for file_name in potential_matches:
            parts = file_name.split('(')
            if len(parts) == 1:
                # Then there is no file version
                continue
            ints.append(int(parts[1].split(')')[0]))
        if not ints:
            files_I_want.append(f'{market_file}.csv')
        else:
            files_I_want.append(f'{market_file} ({max(ints)}).csv')
    return files_I_want


class Holdings:
    @staticmethod
    def get_df(downloads_folder_files, df):
        td_fname = Holdings.get_latest_file(downloads_folder_files, Fname.td_in)
        if td_fname is None:
            return None
        td_df = pd.read_csv(Folder.downloads + td_fname, header=7)
        td_df = Holdings.prepare_df(td_df)
        td_df = td_df.merge(df[['symbol', 'rs rating', M.cur_price, M.days_til_earnings]], on='symbol', how='left')
        # td_df['money val'] = td_df[M.cur_price] - td_df['strike price'].astype(float)
        # td_df['time val'] = td_df['market value'] / 100 - td_df['money val']
        td_df[M.cur_price] = td_df[M.cur_price].astype(float)
        td_df['cost/q'] = (td_df['book cost'].astype(float) / td_df['quantity'].astype(float)) / 100
        td_df['even pt'] = td_df['strike price'].astype(float) + td_df['cost/q']
        td_df['% safe'] = ((td_df[M.cur_price] - td_df['even pt']) / td_df[M.cur_price] * 100).round(0)
        td_df = td_df.drop(['even pt'], axis=1)
        td_df.drop(['book cost', 'current price', 'strike price'], axis=1).round(2).to_csv(Fname.td_out, index=False)
        return td_df

    @staticmethod
    def prepare_df(df):
        df.columns = df.columns.str.lower()
        parts = df['symbol'].str.split()
        df['symbol'] = [part[0] for part in parts]
        df['expiry date'] = [part[2] for part in parts]
        df['strike price'] = [part[3] for part in parts]
        df['days until expiry'] = (pd.to_datetime(pd.Series(df['expiry date']),
                                                  format="%d%b%y") - dt2.now()).dt.days + 1
        # Only keep columns where quantity is greater than 0
        df = df[df['quantity'] > 0]
        df = df[HOLDING_COLS]
        return df

    @staticmethod
    def get_latest_file(downloads_folder_files, f_part_name='-holdings-'):
        """
        Need to first find the TD file with the latest date
        Once we have the moste recent file according to date
        Then need to find the latest file in the day
        :param downloads_folder_files:
        :param f_part_name:
        :return:
        """
        matches = [s for s in downloads_folder_files if f_part_name.lower() in s.lower()]
        try:
            item = matches[0]
        except IndexError:
            return None
        item_parts = item.split('-')
        date_str = f'{item_parts[2]}-{item_parts[3]}-{item_parts[4][:4]}'
        latest_date = dt2.strptime(date_str, "%d-%b-%Y")
        for item in matches[1:]:
            item_parts = item.split('-')
            date_str = f'{item_parts[2]}-{item_parts[3]}-{item_parts[4][:4]}'
            date = dt2.strptime(date_str, "%d-%b-%Y")
            if date > latest_date:
                latest_date = date
        latest_date_str = latest_date.strftime("%d-%b-%Y")
        match_str = latest_date_str + '.csv'
        match = [s for s in downloads_folder_files if match_str.lower() in s.lower()][0]
        match_no_file_ext = match[:-4]
        return find_latest_files_in_folder(downloads_folder_files, match_no_file_ext)[0]


def create_buy_df(df: pd.DataFrame, output_cols: list, screeners: list) -> pd.DataFrame:
    for screener in screeners:
        df = screener.apply(df)


def create_buy_csv_file(df: pd.DataFrame, fname: str, output_cols: list) -> pd.DataFrame:
    """
    Given df, apply screener, make pretty, write specific columns to fname and return buy_df
    :param df: Dataframe
    :param fname: File to write to
    :param output_cols: Columns of df to write to file
    :return: buy_df
    """
    df = df.sort_values('total score', ascending=False)
    df = df.round(2)
    df = df[output_cols]
    df.to_csv(fname, index=False)
    return df


def join_list(lst):
    try:
        return ", ".join(lst)
    except TypeError:
        return ""


def add_pivot_columns(df):
    pivot_df = pd.read_csv(Fname.marketsmith_recent)
    df = df.merge(pivot_df, on='symbol', how='left')
    df['days since pivot'] = df['days since pivot'].fillna(-1)
    return df


def get_iex_df(symbols, settings):
    try:
        iex_df = pd.read_csv('tables/iex analysts.csv')
        iex_df['time created'] = pd.to_datetime(iex_df['time created'])
    except EmptyDataError:
        iex_df = pd.DataFrame()
    if settings.GET_NEW_DATA:
        api = API(settings.GET_REAL_PAID_DATA)
        # analyst_df = api.get_latest_analyst_recommendations_as_series(symbols)
        analyst_df = pd.DataFrame()  # stop getting analyst data for now because we're not using it
        target_df = api.get_price_target(symbols)
        if analyst_df.empty and target_df.empty:
            return iex_df
        elif analyst_df.empty:
            new_iex_df = target_df
        elif target_df.empty:
            new_iex_df = analyst_df
        else:
            new_iex_df = analyst_df[['symbol', 'ratingScaleMark', 'valid since', 'total_ratings']].merge(target_df[
                                                                                                             ['symbol',
                                                                                                              'priceTargetAverage',
                                                                                                              'priceTargetHigh',
                                                                                                              'priceTargetLow',
                                                                                                              'numberOfAnalysts',
                                                                                                              'updatedDate',
                                                                                                              'time '
                                                                                                              'created']],
                                                                                                         on='symbol',
                                                                                                         how='outer')
        if 'time created' in new_iex_df.columns:
            new_iex_df['time created'] = pd.to_datetime(
                new_iex_df['time created'].fillna((dt2.now() - timedelta(days=365))))
        if iex_df.empty:
            iex_df = new_iex_df
        else:
            # Add the new values to end of dataframe, if duplicates exist, keep the latest values
            iex_df.columns = iex_df.columns.str.lower()
            new_iex_df.columns = new_iex_df.columns.str.lower()
            iex_df = pd.concat([iex_df, new_iex_df]).drop_duplicates('symbol', keep='last')
        if settings.GET_REAL_PAID_DATA:
            iex_df.to_csv('tables/iex analysts.csv', index=False)
    else:
        return iex_df
    return iex_df


def add_columns_to_df(df):
    df[M.ad_rating_int] = df.replace({M.ad_rating: LET_TO_NUM})[M.ad_rating]
    # TODO: change into days away from earnings
    df['days until earnings'] = (pd.to_datetime(df[M.eps_due_date]) - dt2.now()).dt.days
    df.loc[df['days until earnings'] > 21, 'days until earnings'] = None
    df['days since earnings'] = (dt2.now() - pd.to_datetime(df[M.eps_last_reported])).dt.days
    df.loc[df['days since earnings'] > 7, 'days since earnings'] = None
    df = add_pivot_columns(df)
    return df
