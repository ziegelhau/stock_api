import operator
from enum import Enum

from analysis.utils import Criteria
from misc.constants import Metric as M, ONEIL_COLS, ONEIL_CRITERIA
from misc.settings import MIN_RS_RATING


class Screener(Enum):
    @property
    def col_name(self):
        return self.value[0]

    @property
    def val(self):
        return self.value[1]


class BuyScreener(Screener):
    min_rs_rating = ('rs rating', MIN_RS_RATING)
    min_comp_rating = ('comp rating', 80)  # originally 90
    min_price = ('current price', 10)
    min_pct_chg_12_m = (M.pct_chg_12_m, 0)

    @staticmethod
    def apply(buy_df):
        buy_df = buy_df[buy_df[BuyScreener.min_comp_rating.col_name] >= BuyScreener.min_comp_rating.val]
        buy_df = buy_df[buy_df[BuyScreener.min_rs_rating.col_name] >= BuyScreener.min_rs_rating.val]
        buy_df = buy_df[buy_df[BuyScreener.min_price.col_name] >= BuyScreener.min_price.val]
        buy_df = buy_df[buy_df[BuyScreener.min_pct_chg_12_m.col_name] >= BuyScreener.min_pct_chg_12_m.val]
        return buy_df


class OneilScreener(Screener):
    @staticmethod
    def apply(df):
        Criteria('eps rating ≥ 80', M.eps_rating, 79, operator.gt)
        Criteria('rs rating ≥ 80', M.rs_rating, 79, operator.gt)
        Criteria('a/d rating a,b,c', M.ad_rating_int, 1, operator.gt)
        Criteria('eps % chg last qtr (-/+) ≥ 20', M.eps_per_chg_lst_qtr, 19.9, operator.gt)
        Criteria('sales % chg lst qtr ≥ 20', M.sales_per_chg_lst_qtr, 19.9, operator.gt)
        Criteria('current price ≥ 10', M.cur_price, 9.9, operator.gt)
        Criteria('% off high ≥ -15', '% off high', -16, operator.gt)
        Criteria('50-day avg vol (1000s) ≥ 100', M.vol_50_day_avg, 99, operator.gt)
        Criteria('number of funds ≥ 5', M.num_funds, 4, operator.gt)
        scaling = 'step'
        for criteria in Criteria.s:
            df[f'{criteria.name}'] = criteria.apply(df[criteria.col], scaling)
        name = 'oneil'
        cols = ONEIL_COLS
        df[f'{name}_pass'] = df[cols].sum(axis=1)
        df[f'{name}_fail'] = len(cols) - df[f'{name}_pass'] - df[cols].isna().sum(axis=1)
        df[f'{name}_score'] = df[f'{name}_pass'] / (df[f'{name}_pass'] + df[f'{name}_fail'])
        df = df[df['oneil_score'] >= ONEIL_CRITERIA]
        return df
